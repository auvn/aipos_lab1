package com.band.ftpclient.observers;

import java.util.ArrayList;
import java.util.List;

/**
 * ����� ��� ������ � "�������������"
 * 
 * @version 0.1
 */
public class DataSocketInputDataObserver implements Observable {
	/** ������ "������������" */
	private List<Observer> observersList;

	/**
	 * ������������� ������ "������������"
	 */
	public DataSocketInputDataObserver() {
		createObserversList();
	}

	/**
	 * ��������� � ������ "������������"
	 * {@link DataSocketInputDataObserver#observersList} ������
	 * 
	 * @param observer
	 *            - ������ � ��������� {@link Observer}
	 */
	public void addObserver(Observer observer) {
		observersList.add(observer);
	}

	/**
	 * ���������� ���� �� {@link DataSocketInputDataObserver#observersList} �
	 * ������������ �������
	 * 
	 */
	public void notifyAllObservers() {
		for (Observer observer : observersList) {
			observer.update();
		}
	}

	/**
	 * ������� �� ������ "������������"
	 * {@link DataSocketInputDataObserver#observersList}
	 * 
	 * @param observer
	 *            - ������ � ��������� {@link Observer}
	 */
	public void removeObserver(Observer observer) {
		observersList.remove(observer);
	}

	/**
	 * ������� ������ ������ "������������"
	 * {@link DataSocketInputDataObserver#observersList}
	 * 
	 */
	private void createObserversList() {
		observersList = new ArrayList<Observer>();
	}

}
