package com.band.ftpclient.gui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JMenuBar;

import com.band.ftpclient.gui.menubars.mainframe.MainMenuBar;
import com.band.ftpclient.gui.panels.ftpviewpanel.FtpViewPanel;
import com.band.ftpclient.gui.panels.mainpanel.MainPanel;
import com.band.ftpclient.gui.panels.textviewpanel.TextViewPanel;

/**
 * ������� ���� ����������
 * 
 * @version 0.1
 */
public class MainFrame extends JFrame {

	/** ������� ���� (������������ ������ ������ {@link MainFrame} */
	private static MainFrame mainFrame;
	/** ������ ���� */
	private JMenuBar mainFrameMenuBar;
	/** ������� ������ ���� */
	private MainPanel mainPanel;
	/** ������ ��� ����������� ������� � �������� */
	private TextViewPanel textViewPanel;
	/** ������ ��� ����������� ������� � �������� */
	private FtpViewPanel ftpViewPanel;
	/** ���������� ���� ���������� */
	private ConnectingFrame connectingFrame;

	/**
	 * �������������� � ��������� ��� �������� �������� ����
	 */
	private MainFrame() {
		createMainPanel();
		createMainFrameMenuBar();

		createTextViewPanel();
		createFtpViewPanel();

		setProperties();

		setVisible(true);
	}

	/**
	 * ������� ������ ������ {@link MainFrame}, ���� �� �� ��� ������, �����
	 * ���������� ������������ {@link MainFrame#mainFrame}
	 * 
	 * @return {@link MainFrame#mainFrame}
	 */
	public static MainFrame getMainFrame() {
		if (mainFrame == null) {
			mainFrame = new MainFrame();
		}
		return mainFrame;
	}

	/**
	 * ������������� �������� �������� ����
	 */
	private void setProperties() {
		setSize(new Dimension(500, 500));
		setTitle(new String("FTPClient v0.1"));
		setLocation(
				(int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() - getWidth()) / 2,
				(int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() - getHeight()) / 2);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * ������� ������ ����
	 */
	private void createMainFrameMenuBar() {
		mainFrameMenuBar = new MainMenuBar();
		setJMenuBar(mainFrameMenuBar);
	}

	/**
	 * ������� ������� ������
	 */
	private void createMainPanel() {
		mainPanel = new MainPanel();
		setContentPane(mainPanel);
	}

	/**
	 * ������� ������ ����������� �������
	 */
	private void createTextViewPanel() {
		textViewPanel = new TextViewPanel();
		mainPanel.addTab(new String("Text ftp view"), textViewPanel);
	}

	/**
	 * ������� ������ ����������� �������
	 */
	private void createFtpViewPanel() {
		ftpViewPanel = new FtpViewPanel();
		mainPanel.addTab(new String("Content ftp view"), ftpViewPanel);
	}

	/**
	 * getter ��� {@link MainFrame#mainPanel}
	 * 
	 * @return {@link MainFrame#mainPanel}
	 */
	public MainPanel getMainPanel() {
		return mainPanel;
	}

	/**
	 * getter ��� {@link MainFrame#textViewPanel}
	 * 
	 * @return {@link MainFrame#textViewPanel}
	 */

	public TextViewPanel getTextViewPanel() {
		return textViewPanel;
	}

	/**
	 * getter ��� {@link MainFrame#ftpViewPanel}
	 * 
	 * @return {@link MainFrame#ftpViewPanel}
	 */
	public FtpViewPanel getFtpViewPanel() {
		return ftpViewPanel;
	}

	/**
	 * getter ��� {@link MainFrame#connectingFrame}
	 * 
	 * @return {@link MainFrame#connectingFrame}
	 */
	public ConnectingFrame getConnectingFrame() {
		return connectingFrame;
	}

	/**
	 * setter ��� {@link MainFrame#connectingFrame}
	 * 
	 * @param connectingFrame
	 */
	public void setConnectingFrame(ConnectingFrame connectingFrame) {
		this.connectingFrame = connectingFrame;
	}
}
