package com.band.ftpclient.gui.panels.textviewpanel;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;

/**
 * ��������� ������� enter � ��������� ����
 * @version 0.1
 */
public class TextViewPanel extends JPanel {
	
	/** ���� ��� ����� ������ */
	private JTextField commandTextField;
	/** ������� ��� ������ ������ */
	private JTextArea outputTextArea;
	/** ���� ������� � ������� ��� ������ ������ */
	private DefaultCaret outputTextAreaCaret;

	/**
	 * ������������� �������� �������.
	 * ������� � ��������� ���� ��� ����� � ������� ������ ������
	 */
	public TextViewPanel() {
		setProperties();

		createOutputTextArea();
		createCommandTextField();
	}

	/**
	 * ������������� �������� �������
	 */
	private void setProperties() {
		setLayout(new BorderLayout());
	}

	/**
	 * ������� � ��������� �� ������� ������ ���� ��� ����� ������
	 */
	private void createCommandTextField() {
		commandTextField = new JTextField();
		add(commandTextField, BorderLayout.SOUTH);
	}

	/**
	 * ������� � ��������� �� ������� ������ ������� ������ ������
	 */
	private void createOutputTextArea() {
		outputTextArea = new JTextArea();
		outputTextArea.setLineWrap(true);
		outputTextArea.setEditable(false);
		outputTextAreaCaret = (DefaultCaret) outputTextArea.getCaret();
		outputTextAreaCaret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		add(new JScrollPane(outputTextArea), BorderLayout.CENTER);
	}

	/**
	 * getter ��� {@link TextViewPanel#outputTextArea}
	 * @return {@link TextViewPanel#outputTextArea}
	 */
	public JTextArea getOutputTextArea() {
		return outputTextArea;
	}

	/**
	 * getter ��� {@link TextViewPanel#commandTextField}
	 * @return {@link TextViewPanel#commandTextField}
	 */
	public JTextField getCommandTextField() {
		return commandTextField;
	}
}
