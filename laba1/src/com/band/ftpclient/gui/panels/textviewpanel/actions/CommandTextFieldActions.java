package com.band.ftpclient.gui.panels.textviewpanel.actions;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;

import com.band.ftpclient.connection.Connection;
import com.band.ftpclient.gui.MainFrame;

/**
 * ��������� ������� ������ � ��������� ����
 * 
 * @version 0.1
 */
public class CommandTextFieldActions implements KeyListener {

	/** ���� ��� ����� ������ */
	private JTextField textField;

	/**
	 * ��� ������� ������� enter �������� ������� �������
	 */
	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyCode() == 10) {
			textField = (JTextField) arg0.getSource();
			MainFrame.getMainFrame().getTextViewPanel().getOutputTextArea()
					.append(textField.getText() + "\n");

			Connection connection = Connection.getConnection();
			connection.send(textField.getText());
			textField.setText(new String());
		}
	}

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}
