package com.band.ftpclient.gui.panels.ftpviewpanel.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import com.band.ftpclient.connection.Connection;
import com.band.ftpclient.data.FtpViewTreeData;

public class FtpViewTreeActions implements MouseListener {
	/** ������, �� �������� ��������� ������� */
	private JTree ftpViewTree;
	/** ���������� ���������� */
	private Connection connection;
	/** ������, ���������� ������� Path ������ */
	private String curPathStr;
	/** ������ �� ����� ��������� ������ ��� ���������� ������ */
	private FtpViewTreeData ftpViewTreeData;
	/** ������� ���� ������ */
	private DefaultMutableTreeNode curNode;

	/**
	 * ������������� ����������� ����������
	 * 
	 * @param ftpViewTreeData
	 *            - ������ ���� {@link FtpViewTreeData}
	 */
	public FtpViewTreeActions(FtpViewTreeData ftpViewTreeData) {
		this.ftpViewTreeData = ftpViewTreeData;
		this.connection = Connection.getConnection();
	}

	/**
	 * ���������� ������� ��� ������� ������� �� ������� ���� �� ����������
	 * {@link MouseListener}
	 * 
	 */
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			setFtpViewTree((JTree) e.getSource());
			curNode = (DefaultMutableTreeNode) ftpViewTree
					.getLastSelectedPathComponent();

			if (ftpViewTree.getSelectionPath() != null
					&& curNode.getChildCount() == 0) {
				curPathStr = ftpViewTree.getSelectionPath().toString()
						.replaceAll(", ", "/").replaceAll("\\[|\\]", "");

				if (curNode.getAllowsChildren()) {

					connection.send("PASV");
					connection.send("LIST " + curPathStr);

					ftpViewTree.setEnabled(false);
					ftpViewTreeData.setListRequest(true);
				} else {

					if (JOptionPane.showConfirmDialog(null,
							"You really want to download a file?\n" + curPathStr,
							"Message", JOptionPane.YES_NO_OPTION,
							JOptionPane.INFORMATION_MESSAGE) == JOptionPane.YES_OPTION) {
						connection.send("PASV");
						connection.send("RETR " + curPathStr);
					}

				}
			}
		}

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {

	}

	/**
	 * setter ��� {@link FtpViewTreeActions#ftpViewTree}. ����������� ����������
	 * {@link FtpViewTreeActions#ftpViewTree} �� �������� {@link JTree}
	 * ��������� ������� {@link MouseEvent}
	 * 
	 * @param ftpViewTree
	 *            - JTree �� �������� ��������� ������� {@link MouseEvent}
	 * 
	 */
	private void setFtpViewTree(JTree ftpViewTree) {
		if (this.ftpViewTree == null)
			this.ftpViewTree = ftpViewTree;
	}

}
