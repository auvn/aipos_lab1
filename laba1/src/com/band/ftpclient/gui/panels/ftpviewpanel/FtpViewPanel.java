package com.band.ftpclient.gui.panels.ftpviewpanel;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * ������� ���������� ��������� �� �������
 * 
 * @version 0.1
 */
public class FtpViewPanel extends JPanel {
	/** ������ ��� ����������� ��������� */
	private JTree ftpViewTree;

	/**
	 * ������������� ��������
	 */
	public FtpViewPanel() {
		setProperties();

		createFtpViewTree();
	}

	/**
	 * ������������� ��� ���� ��� ���������� ��������
	 */
	private void setProperties() {
		setLayout(new BorderLayout());
	}

	/**
	 * �������� {@link FtpViewPanel#ftpViewTree} ��� ����������� ���������
	 */
	private void createFtpViewTree() {
		ftpViewTree = new JTree(new DefaultMutableTreeNode("[/]", true));
		add(new JScrollPane(ftpViewTree), BorderLayout.CENTER);
		ftpViewTree.setEnabled(false);
	}

	/**
	 * getter ��� {@link FtpViewPanel#ftpViewTree}
	 * 
	 * @return {@link FtpViewPanel#ftpViewTree}
	 */
	public JTree getFtpViewTree() {
		return ftpViewTree;
	}

}
