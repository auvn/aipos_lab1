package com.band.ftpclient.gui.panels.connectingpanel;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

/**
 * ������ ����������� ���� "Connection"
 * @version 0.1
 */
public class ConnectingPanel extends JPanel {
	
	/** ����� "URL" */
	private JLabel urlLabel;
	/** ����� "Login" */
	private JLabel loginLabel;
	/** ����� "Password" */
	private JLabel passwordLabel;

	/** ���� ��� ����� URL */
	private JTextField urlTextField;
	/** ���� ��� ����� login-� */
	private JTextField loginTextField;
	/** ���� ��� ����� ������ */
	private JTextField passwordTextField;

	/** ������ �� */
	private JButton okButton;
	/** ������ Cancel */
	private JButton cancelButton;

	/**
	 * ������� � ��������� ��� �������� �� ������
	 */
	public ConnectingPanel() {
		createLabels();
		createTextFields();
		createButtons();
		createLayout();
	}

	/**
	 * ������� �����
	 */
	private void createLabels() {
		urlLabel = new JLabel("URL: ");
		urlLabel.setLabelFor(urlTextField);

		loginLabel = new JLabel("Login: ");
		loginLabel.setLabelFor(loginTextField);

		passwordLabel = new JLabel("Password: ");
		passwordLabel.setLabelFor(passwordTextField);
	}

	/**
	 * ������� ��������� ����
	 */
	private void createTextFields() {
		urlTextField = new JTextField();
		urlTextField.setText("ftp.bynets.org");

		loginTextField = new JTextField();
		loginTextField.setText("anonymous");

		passwordTextField = new JTextField();
		passwordTextField.setText("anonymous");
	}

	/**
	 * ������� ������
	 */
	private void createButtons() {
		okButton = new JButton("OK");
		cancelButton = new JButton("Cancel");
	}

	/**
	 * ������� GridBag-���� � ��������� �� ��� ��� �������� ������
	 */
	private void createLayout() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;

		constraints.gridy = 0;
		add(urlLabel, constraints);

		constraints.gridy = 1;
		add(loginLabel, constraints);

		constraints.gridy = 2;
		add(passwordLabel, constraints);

		constraints.gridx = 1;
		constraints.gridwidth = 2;

		constraints.gridy = 0;
		add(urlTextField, constraints);

		constraints.gridy = 1;
		add(loginTextField, constraints);

		constraints.gridy = 2;
		add(passwordTextField, constraints);

		constraints.gridy = 3;
		constraints.gridwidth = 1;
		add(okButton, constraints);

		constraints.gridx = 2;
		add(cancelButton, constraints);
	}

	/**
	 * ����� �����, ��������� � {@link ConnectingPanel#urlTextField}
	 * @return �����, ��������� � {@link ConnectingPanel#urlTextField}
	 */
	public String getInputUrl() {
		return urlTextField.getText();
	}

	/**
	 * ����� �����, ��������� � {@link ConnectingPanel#loginTextField}
	 * @return �����, ��������� � {@link ConnectingPanel#loginTextField}
	 */
	public String getInputLogin() {
		return loginTextField.getText();
	}

	/**
	 * ����� �����, ��������� � {@link ConnectingPanel#passwordTextField}
	 * @return �����, ��������� � {@link ConnectingPanel#passwordTextField}
	 */
	public String getInputPassword() {
		return passwordTextField.getText();
	}

	/**
	 * getter ��� {@link ConnectingPanel#okButton}
	 * @return {@link ConnectingPanel#okButton}
	 */
	public JButton getOkButton() {
		return okButton;
	}

	/**
	 * getter ��� {@link ConnectingPanel#cancelButton}
	 * @return {@link ConnectingPanel#cancelButton}
	 */
	public JButton getCancelButton() {
		return cancelButton;
	}

	/**
	 * ������� ���� ��� �����
	 */
	public void cleanTextFields() {
		urlTextField.setText(new String());
		loginTextField.setText(new String());
		passwordTextField.setText(new String());
	}
}
