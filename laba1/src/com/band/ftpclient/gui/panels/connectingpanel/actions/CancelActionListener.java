package com.band.ftpclient.gui.panels.connectingpanel.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.band.ftpclient.gui.ConnectingFrame;

/**
 * ������� ������ Cancel � ���������� ���� "Connection"
 * @version 0.1
 */
public class CancelActionListener implements ActionListener {

	/** ���������� ���� ���������� */
	private ConnectingFrame connectingFrame;

	/**
	 * �������� ������ �� ���������� ���� ����������
	 * @param connectingFrame ���������� ���� ����������
	 */
	public CancelActionListener(ConnectingFrame connectingFrame) {
		this.connectingFrame = connectingFrame;
	}

	/**
	 * ��������� ���������� ���� ����������
	 */
	public void actionPerformed(ActionEvent arg0) {
		connectingFrame.setVisible(false);
	}
}
