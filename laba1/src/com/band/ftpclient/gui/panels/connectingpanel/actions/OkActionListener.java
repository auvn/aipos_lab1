package com.band.ftpclient.gui.panels.connectingpanel.actions;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextArea;

import com.band.ftpclient.connection.Connection;
import com.band.ftpclient.gui.ConnectingFrame;
import com.band.ftpclient.gui.MainFrame;
import com.band.ftpclient.gui.panels.connectingpanel.ConnectingPanel;

/**
 * ������� ������ OK � ���������� ���� "Connection"
 * @version 0.1
 */
public class OkActionListener implements ActionListener {

	/** ���������� ���� ���������� */
	private ConnectingFrame connectingFrame;	
	/** ������, ���������� �� ������������ �� ����������� ���� ���������� */
	private String url, login, password;
	/** ��������� ���� ��� log-� */
	private JTextArea outputTextArea;
	/** ������ ����������� ���� ���������� */
	private ConnectingPanel connectingPanel;

	/**
	 * �������� ������ �� ���������� ���� ����������
	 * @param connectingFrame ���������� ���� ����������
	 */
	public OkActionListener(ConnectingFrame connectingFrame) {
		this.connectingFrame = connectingFrame;
		outputTextArea = MainFrame.getMainFrame().getTextViewPanel()
				.getOutputTextArea();
		connectingPanel = connectingFrame.getConnectingPanel();
	}

	/**
	 * ������������� ���������� �� ��������� ������������� ������
	 */
	public void actionPerformed(ActionEvent e) {
		url = connectingPanel.getInputUrl();
		login = connectingPanel.getInputLogin();
		password = connectingPanel.getInputPassword();
		outputTextArea.append("Connecting to " + url + "\n");
		outputTextArea.append("Login: " + login + "\n");
		outputTextArea.append("Password: " + password + "\n");
		Connection.getConnection().connect(url, login, password);
		connectingFrame.setVisible(false);
	}
}
