package com.band.ftpclient.gui.panels.mainpanel;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * ������� ������
 * @version 0.1
 */
public class MainPanel extends JPanel {
	
	/** Tab-������ */
	private JTabbedPane mainTabbedPane;

	/**
	 * ������������� �������� ������� � ������� Tab-������
	 */
	public MainPanel() {
		setProperties();
		createMainTabbedPane();
	}

	/**
	 * ������������� border-����
	 */
	private void setProperties() {
		setLayout(new BorderLayout());
	}

	/**
	 * ������� � ��������� �� ������� ������ Tab-������
	 */
	private void createMainTabbedPane() {
		mainTabbedPane = new JTabbedPane();
		add(mainTabbedPane, BorderLayout.CENTER);
	}

	/**
	 * ��������� �� Tab-������ ����� �������
	 * @param title �������� �������
	 * @param panel ������ �� �������
	 */
	public void addTab(String title, JPanel panel) {
		mainTabbedPane.addTab(title, panel);
	}
}
