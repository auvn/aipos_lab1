package com.band.ftpclient.gui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import com.band.ftpclient.gui.panels.connectingpanel.ConnectingPanel;

/**
 * ���������� ���� "Connection"
 * @version 0.1
 */
public class ConnectingFrame extends JFrame {

	/** ������ ����������� ���� Connection */
	private ConnectingPanel connectingPanel;

	/**
	 * ������������� �������� �������,
	 * ������� ������ ����������
	 * @param mainFrame
	 */
	public ConnectingFrame(MainFrame mainFrame) {
		createConnectingPanel();
		setProperties();
	}

	/**
	 * ������� ������ ����������
	 */
	private void createConnectingPanel() {
		connectingPanel = new ConnectingPanel();
		setContentPane(connectingPanel);
	}

	/**
	 * ������������� �������� �������
	 */
	private void setProperties() {
		setSize(new Dimension(200, 120));
		setResizable(false);
		setTitle(new String("FTPClient v0.0 -> Connecting"));
		setLocation(
				(int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() - getWidth()) / 2,
				(int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() - getHeight()) / 2);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * getter ��� {@link ConnectingFrame#connectingPanel}
	 * @return {@link ConnectingFrame#connectingPanel}
	 */
	public ConnectingPanel getConnectingPanel() {
		return connectingPanel;
	}
}
