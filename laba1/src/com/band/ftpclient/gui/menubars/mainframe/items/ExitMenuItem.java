package com.band.ftpclient.gui.menubars.mainframe.items;

import javax.swing.JMenuItem;

import com.band.ftpclient.gui.menubars.mainframe.items.actions.ExitAction;

/**
 * ����� ���� File -> Exit
 * @version 0.1
 */
public class ExitMenuItem extends JMenuItem {

	/**
	 * ������������� �������� ������� � ��������� ����������� �������
	 */
	public ExitMenuItem() {
		setProperties();
		addListeners();
	}

	/**
	 * ������������� �������� �������
	 */
	private void setProperties() {
		setText(new String("Exit"));
	}

	/**
	 * ��������� ����������� ������� Exit
	 */
	private void addListeners() {
		addActionListener(new ExitAction());
	}
}
