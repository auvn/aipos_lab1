package com.band.ftpclient.gui.menubars.mainframe;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import com.band.ftpclient.gui.menubars.mainframe.items.ConnectMenuItem;
import com.band.ftpclient.gui.menubars.mainframe.items.ExitMenuItem;

/**
 * ���� "File"
 * @version 0.1
 */
public class FileMenu extends JMenu {
	
	/** ����� ���� "File -> Exit" */
	private JMenuItem exitMenuItem;
	/** ����� ���� "File -> Connect" */
	private JMenuItem connectMenuItem;

	/**
	 * ������������� �������� ������� � ��������� ������ ����
	 */
	public FileMenu() {
		setProperties();
		createConnectMenuItem();
		addSeparator();
		createExitMenuItem();
	}

	/**
	 * ������������� �������� �������
	 */
	private void setProperties() {
		setText(new String("File"));
	}

	/**
	 * ��������� ����� ���� {@link FileMenu#connectMenuItem}
	 */
	private void createConnectMenuItem() {
		connectMenuItem = new ConnectMenuItem();
		add(connectMenuItem);
	}

	/**
	 * ��������� ����� ���� {@link FileMenu#exitMenuItem}
	 */
	private void createExitMenuItem() {
		exitMenuItem = new ExitMenuItem();
		add(exitMenuItem);
	}
}
