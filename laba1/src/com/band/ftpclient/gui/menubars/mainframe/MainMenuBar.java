package com.band.ftpclient.gui.menubars.mainframe;

import javax.swing.JMenu;
import javax.swing.JMenuBar;

/**
 * ������� ������ ����
 * @version 0.1
 */
public class MainMenuBar extends JMenuBar {
	
	/** ���� "File"*/
	private JMenu fileMenu;

	/**
	 * ������� � ��������� ���� "File"
	 */
	public MainMenuBar() {
		createFileMenu();
		add(fileMenu);
	}

	/**
	 * ������� ���� "File"
	 */
	private void createFileMenu() {
		fileMenu = new FileMenu();
	}
}
