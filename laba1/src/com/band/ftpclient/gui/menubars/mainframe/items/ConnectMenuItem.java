package com.band.ftpclient.gui.menubars.mainframe.items;

import javax.swing.JMenuItem;

import com.band.ftpclient.gui.menubars.mainframe.items.actions.ConnectAction;

/**
 * ����� ���� File -> Connect
 * @version 0.1
 */
public class ConnectMenuItem extends JMenuItem {
	
	/**
	 * ������������� �������� ������� � ��������� ����������� �������
	 */
	public ConnectMenuItem() {
		setProperties();
		addListeners();
	}

	/**
	 * ������������� �������� �������
	 */
	private void setProperties() {
		setText(new String("Connect"));
	}

	/**
	 * ��������� ����������� ������� Connect
	 */
	private void addListeners() {
		addActionListener(new ConnectAction());
	}
}
