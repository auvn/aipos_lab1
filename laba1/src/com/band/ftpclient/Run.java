package com.band.ftpclient;

/**
 * Запуск приложения
 * 
 * @version 0.1
 */
public class Run {
	/**
	 * Стартовая точка приложения
	 * 
	 * @param args
	 *            аргументы
	 */
	public static void main(String[] args) {
		startApp();
	}

	/**
	 * Запуск запланированных действий
	 */
	public static void startApp() {
		AllActions.init();
		AllActions.test();
	}
}