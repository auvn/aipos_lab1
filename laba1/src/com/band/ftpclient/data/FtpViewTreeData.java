package com.band.ftpclient.data;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import com.band.ftpclient.connection.UserCommandsListener;
import com.band.ftpclient.gui.MainFrame;
import com.band.ftpclient.observers.Observer;

/**
 * ������������ ���������� ������ �� ��������� � ������ FTP-�������
 * 
 * @version 0.1
 */
public class FtpViewTreeData implements Observer {
	/** ����� ��������, ��� ��� ������ ������ ������ � ���������� */
	private boolean listRequest = false;
	/** ���������� �������������� ������� �� ������������ */
	private UserCommandsListener userCommandsListener;
	/** ������, � ������� ���������� �������� ����� ����� */
	private JTree ftpViewTree;
	/** ������� ���� */
	private DefaultMutableTreeNode curNode;
	/** ����� ���������� ���������� �������� ������ */
	private boolean allowsChildren = false;
	/** ������� ��� ��������� ������ �� ������� */
	private Scanner listRequestStrScanner;
	/** RegExp ��� ��������� ������ ������ �� ������� */
	private Pattern listLinePattern;
	/** */
	private Matcher listLineMatcher;

	/**
	 * ������������� ����������� ��� ������ ����������
	 * 
	 * @param userCommandsListener
	 *            �������������� ������ �� ������������
	 * @param mainFrame
	 *            ������� ����� ����������
	 */
	public FtpViewTreeData(UserCommandsListener userCommandsListener,
			MainFrame mainFrame) {
		this.userCommandsListener = userCommandsListener;
		this.ftpViewTree = mainFrame.getFtpViewPanel().getFtpViewTree();
		listLinePattern = Pattern
				.compile("(.*)    (.*)\\ *(\\d*)\\ *(\\d*) (.*) (.*)");
	}

	/**
	 * �������� ���������� ������ �� ������� ������ � �������
	 */
	public void update() {
		if (listRequest) {
			listRequestStrScanner = new Scanner(userCommandsListener
					.getListCommandStringBuffer().toString());

			curNode = (DefaultMutableTreeNode) ftpViewTree
					.getLastSelectedPathComponent();

			while (listRequestStrScanner.hasNext()) {
				listLineMatcher = listLinePattern.matcher(listRequestStrScanner
						.nextLine());
				if (listLineMatcher.find()) {
					allowsChildren = listLineMatcher.group(1).startsWith("d");
					curNode.add(new DefaultMutableTreeNode(allowsChildren ? "["
							+ listLineMatcher.group(6) + "]" : listLineMatcher
							.group(6), allowsChildren));
				}
			}
			ftpViewTree.expandPath(ftpViewTree.getSelectionPath());
			ftpViewTree.setEnabled(true);
		}
	}

	/**
	 * setter ��� {@link FtpViewTreeData#listRequest}
	 * 
	 * @param listRequest
	 *            ��������, ������� ���������� ����������
	 */
	public void setListRequest(boolean listRequest) {
		this.listRequest = listRequest;
	}

}
