package com.band.ftpclient;

import javax.swing.JTextField;

import com.band.ftpclient.connection.Connection;
import com.band.ftpclient.connection.StreamsThreadsAdaptor;
import com.band.ftpclient.connection.UserCommandsListener;
import com.band.ftpclient.data.FtpViewTreeData;
import com.band.ftpclient.gui.ConnectingFrame;
import com.band.ftpclient.gui.MainFrame;
import com.band.ftpclient.gui.panels.connectingpanel.actions.CancelActionListener;
import com.band.ftpclient.gui.panels.connectingpanel.actions.OkActionListener;
import com.band.ftpclient.gui.panels.ftpviewpanel.actions.FtpViewTreeActions;
import com.band.ftpclient.gui.panels.textviewpanel.actions.CommandTextFieldActions;
import com.band.ftpclient.observers.DataSocketInputDataObserver;

/**
 * ��������������� �������� � ����������
 * 
 * @version 0.1
 */
public class AllActions {

	/** ������� ���� */
	private static MainFrame mainFrame;
	/** ���������� ���� ���������� */
	private static ConnectingFrame connectingFrame;
	/** ���� ����� ������ */
	private static JTextField textField;
	/** ���������� ���������������� ������ */
	private static UserCommandsListener userCommandsListener;
	/** ������� ���������� */
	private static Connection connection;
	/** ������� ������� */
	private static StreamsThreadsAdaptor streamsThreadsAdaptor;
	/** ����������� ��������� ������ �� ������� ������ */
	private static DataSocketInputDataObserver dataSocketInputDataObserver;
	/** ����� ��� ����������� ������ ��������� */
	private static FtpViewTreeData ftpViewTreeData;

	/**
	 * ������������� ��������� ����������
	 */
	public static void init() {
		connection = Connection.getConnection();
		streamsThreadsAdaptor = new StreamsThreadsAdaptor();
		userCommandsListener = new UserCommandsListener();

		connection.setUserCommandsListener(userCommandsListener);
		connection.setStreamsThreadsAdaptor(streamsThreadsAdaptor);

		mainFrame = MainFrame.getMainFrame();
		connectingFrame = new ConnectingFrame(mainFrame);

		mainFrame.setConnectingFrame(connectingFrame);

		dataSocketInputDataObserver = new DataSocketInputDataObserver();

		ftpViewTreeData = new FtpViewTreeData(userCommandsListener, mainFrame);
		dataSocketInputDataObserver.addObserver(ftpViewTreeData);
		userCommandsListener
				.setDataSocketInputDataObserver(dataSocketInputDataObserver);

		textField = mainFrame.getTextViewPanel().getCommandTextField();

		textField.addKeyListener(new CommandTextFieldActions());
		mainFrame.getFtpViewPanel().getFtpViewTree()
				.addMouseListener(new FtpViewTreeActions(ftpViewTreeData));

		connectingFrame.getConnectingPanel().getOkButton()
				.addActionListener(new OkActionListener(connectingFrame));
		connectingFrame.getConnectingPanel().getCancelButton()
				.addActionListener(new CancelActionListener(connectingFrame));

	}

	/**
	 * ������������ ����������
	 */
	public static void test() {
		connection.connect("ftp.bynets.org", "anonymous", "anonymous");
		String pasv = "PASV";
		String list = "LIST";
		connection.send(pasv);
		userCommandsListener.checkCommand(pasv);
		connection.send(list);
		userCommandsListener.checkCommand(list);
	}
}
