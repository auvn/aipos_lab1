package com.band.ftpclient.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.net.ServerSocket;
import java.net.Socket;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import com.band.ftpclient.gui.MainFrame;

/**
 * ������������ ���������� � FTP-�������
 * 
 * @version 0.1
 */
public class Connection {

	/** ���������� (������������ ������ ������ {@link Connection}) */
	private static Connection connection;

	/** ����� ��� ���������� ����������� (���������� ���� 21) */
	private Socket controlSocket;
	/** �������������� ����� ��� �������� ������ */
	private Socket dataSocket;
	/** ����� ��� ��������� ������ ������ */
	private ServerSocket dataServer;

	/** ����� ��� �������� ������ ������� */
	private PrintWriter out;
	/** */
	private StreamsThreadsAdaptor streamsThreadsAdaptor;

	/** RegExp ��� ���������� ������ � ���������� */
	private Pattern pattern;
	/** */
	private Matcher dataPortMatcher;

	/** ���������� �������� ������������� ������ */
	private UserCommandsListener userCommandsListener;

	/** ����� �� ���������� ������ */
	private final int TIMEOUT = 10000;

	/** ������ ������ ������� */
	public enum ConnectionMode {
		Active, Passive
	};

	/**
	 * �������������� RegExp ��� ���������� ������
	 */
	private Connection() {
		createDataPortPattern();
	}

	/**
	 * ������ ������ ������ {@link Connection}, ���� �� �� ��� ������, �����
	 * ���������� ������������ {@link Connection#connection}.
	 * 
	 * @return ������ ������ {@link Connection}
	 */
	public static Connection getConnection() {
		if (connection == null) {
			connection = new Connection();
		}
		return connection;
	}

	/**
	 * ������ RegExp {@link Connection#pattern} ��� ���������� ������
	 */
	private void createDataPortPattern() {
		pattern = Pattern.compile("(\\d+),(\\d+),(\\d+),(\\d+),(\\d+),(\\d+)");
	}

	/**
	 * ��������� ��������� ���������� � FTP-��������
	 * 
	 * @param response
	 *            - ����� �������, ���������� ����� ���������� � �������
	 *            i,i,i,i,i,i
	 */
	public void openPassiveMode(String response) {
		dataPortMatcher = pattern.matcher(response);
		if (dataPortMatcher.find()) {
			String address = dataPortMatcher.group(1) + '.'
					+ dataPortMatcher.group(2) + '.' + dataPortMatcher.group(3)
					+ '.' + dataPortMatcher.group(4);

			int port = Integer.parseInt(dataPortMatcher.group(5)) * 256
					+ Integer.parseInt(dataPortMatcher.group(6));
			try {
				dataSocket = new Socket(address, port);

				userCommandsListener.setDataSocket(dataSocket);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * ��������� �������� ���������� � FTP-��������
	 * 
	 * @param request
	 *            ������ ������������, ���������� ����� ���������� � �������
	 *            i,i,i,i,i,i
	 */
	public void openActiveMode(String request) {
		try {
			dataPortMatcher = pattern.matcher(request);
			if (dataPortMatcher.find()) {
				int port = Integer.parseInt(dataPortMatcher.group(5)) * 256
						+ Integer.parseInt(dataPortMatcher.group(6));
				dataServer = new ServerSocket(port);
				dataServer.setSoTimeout(TIMEOUT);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * ��������� � FTP-�������� � ������������ ������������
	 * 
	 * @param url
	 *            ����� FTP-�������
	 * @param login
	 *            �����
	 * @param password
	 *            ������
	 */
	public void connect(String url, String login, String password) {
		connect(url);
		send("USER " + login);
		send("PASS " + password);
	}

	/**
	 * ��������� � FTP-��������
	 * 
	 * @param urlString
	 *            ����� FTP-�������
	 */
	public void connect(String urlString) {
		try {
			controlSocket = new Socket(urlString, 21);
			streamsThreadsAdaptor
					.startStreams(
							new BufferedReader(new InputStreamReader(
									controlSocket.getInputStream())),
							out = new PrintWriter(controlSocket
									.getOutputStream(), true));
			MainFrame.getMainFrame().getFtpViewPanel().getFtpViewTree()
					.setEnabled(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * ���������� ������ �������
	 * 
	 * @param com
	 *            ������ ������������
	 */
	public void send(String com) {
		userCommandsListener.checkCommand(com);
		out.println(com);
	}

	/**
	 * setter ��� {@link Connection#streamsThreadsAdaptor}
	 * 
	 * @param streamsThreadsAdaptor
	 */
	public void setStreamsThreadsAdaptor(
			StreamsThreadsAdaptor streamsThreadsAdaptor) {
		this.streamsThreadsAdaptor = streamsThreadsAdaptor;
	}

	/**
	 * ��������� ������� ���������� � FTP-��������
	 */
	public void close() {
		try {
			controlSocket.close();
			dataServer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * getter ��� {@link Connection#dataSocket}
	 * 
	 * @return {@link Connection#dataSocket}
	 */
	public Socket getDataSocket() {
		return dataSocket;
	}

	/**
	 * setter ��� {@link Connection#userCommandsListener}
	 * 
	 * @param userCommandsListener
	 */
	public void setUserCommandsListener(
			UserCommandsListener userCommandsListener) {
		this.userCommandsListener = userCommandsListener;
	}

	/**
	 * ��������� ��������� ������ ����� �������������� �����
	 */
	public void allowDataTransfer() {
		userCommandsListener.runUserCommand();
	}

	/**
	 * getter ��� {@link Connection#dataServer}
	 * 
	 * @return {@link Connection#dataServer}
	 */
	public ServerSocket getServerSocket() {
		return dataServer;
	}
}
