package com.band.ftpclient.connection;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import javax.swing.JTextArea;

import com.band.ftpclient.gui.MainFrame;
import com.band.ftpclient.observers.DataSocketInputDataObserver;

/**
 * ������������ ������������� �������� ������������� ������
 * 
 * @version 0.1
 */
public class UserCommandsListener {

	/** �������������� ����� ��� �������� ������ */
	private Socket dataSocket;
	/** ����� ��� ������ ������ */
	private BufferedReader dataInputReader;
	/** ��������� ������� ��� ������ ���� ������� � �������� */
	private JTextArea outputTextArea;
	/** ��������� �������� ������� */
	private String lastTransferCommand;
	/** ������� ������� */
	private String command;
	/** �������� ������ */
	private String tempStr;
	/** ���� ��������� ���������� */
	private boolean activeConnection = false;
	/** ������ ������ ��� �������� �������� ������ */
	public static final int BUFFER_LENGTH = 4096;
	/** �������� ������ ��� �������� ������ ������� LIST */
	private StringBuffer listCommandStringBuffer;
	/** ����������� ��������� ������ �� ������� ������ */
	private DataSocketInputDataObserver dataSocketInputDataObserver;

	/**
	 * �������� ��������� ������ ��� log-�
	 */
	public UserCommandsListener() {
		outputTextArea = MainFrame.getMainFrame().getTextViewPanel()
				.getOutputTextArea();
		listCommandStringBuffer = new StringBuffer();
	}

	/**
	 * ���������� ��������� ��������� ������������� ������� �� ������
	 * ����������� �����
	 * 
	 * @param str
	 *            ���������������� �������
	 */
	public void checkCommand(String str) {
		System.out.println("test!!");
		if (str.length() >= 4) {
			lastTransferCommand = str;
			if (lastTransferCommand.substring(0, 4).toUpperCase()
					.equals("PORT")) {
				Connection.getConnection().openActiveMode(lastTransferCommand);
				activeConnection = true;
			}
		}
	}

	/**
	 * ���������� ��������� ������� �������� ������ ����� �������������� �����
	 */
	public void runUserCommand() {
		if ((dataSocket != null && !dataSocket.isClosed())
				|| activeConnection == true) {
			command = lastTransferCommand.substring(0, 4).toUpperCase();
			switch (command) {
			case "LIST":
				listCommand();
				break;
			case "RETR":
				retrCommand(lastTransferCommand);
				break;
			}
			activeConnection = false;
		}
	}

	/**
	 * ������������ ������� RETR
	 * 
	 * @param str
	 *            ���������������� �������
	 */
	public void retrCommand(String str) {
		int pos = str.lastIndexOf("/");
		if (pos == -1)
			pos = str.lastIndexOf(" ");
		String fileName = str.substring(pos + 1);
		try {
			if (activeConnection == true)
				dataSocket = Connection.getConnection().getServerSocket()
						.accept();
			InputStream ibuf = new BufferedInputStream(
					dataSocket.getInputStream());
			OutputStream obuf = new FileOutputStream(new File(fileName));

			int eof = 0, len;
			byte buf[];

			while (eof != -1) {
				buf = new byte[BUFFER_LENGTH];
				if ((len = ibuf.read(buf, 0, BUFFER_LENGTH)) > 0)
					obuf.write(buf, 0, len);
				eof = len;
			}
			ibuf.close();
			obuf.close();
			dataSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * ������������ ������� LIST
	 */
	public void listCommand() {
		try {
			if (activeConnection == true)
				dataSocket = Connection.getConnection().getServerSocket()
						.accept();
			dataInputReader = new BufferedReader(new InputStreamReader(
					dataSocket.getInputStream()));
			listCommandStringBuffer.setLength(0);
			while ((tempStr = dataInputReader.readLine()) != null)
				listCommandStringBuffer.append(tempStr + '\n');
			outputTextArea.append(listCommandStringBuffer.toString());

			dataSocketInputDataObserver.notifyAllObservers();

			dataSocket.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * ������������ ������� PORT
	 * 
	 * @param str
	 *            ���������������� �������
	 */
	public void portCommand(String str) {
		Connection.getConnection().openActiveMode(str);
		activeConnection = true;
	}

	/**
	 * getter ��� {@link UserCommandsListener#dataSocket}
	 * 
	 * @return {@link UserCommandsListener#dataSocket}
	 */
	public Socket getDataSocket() {
		return dataSocket;
	}

	public StringBuffer getListCommandStringBuffer() {
		return listCommandStringBuffer;
	}

	/**
	 * setter ��� {@link UserCommandsListener#dataSocket}
	 * 
	 * @param dataSocket
	 */
	public void setDataSocket(Socket dataSocket) {
		this.dataSocket = dataSocket;
	}

	/**
	 * setter ��� {@link UserCommandsListener#dataSocketInputDataObserver}
	 * 
	 * @param dataSocketInputDataObserver
	 */
	public void setDataSocketInputDataObserver(
			DataSocketInputDataObserver dataSocketInputDataObserver) {
		this.dataSocketInputDataObserver = dataSocketInputDataObserver;
	}

}
