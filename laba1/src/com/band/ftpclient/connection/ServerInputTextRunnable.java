package com.band.ftpclient.connection;

import java.io.BufferedReader;

import com.band.ftpclient.gui.MainFrame;

/**
 * ������ ������ �������
 * @version 0.1
 */
public class ServerInputTextRunnable implements Runnable {
	
	/** ����� ������ ������ �� ������� */
	private BufferedReader serverInputReader;
	/** ���� ����������� ������������� ������� */
	private boolean state = true;
	/** �������� ������ */
	private String tempStr;
	/** ������� ���� ���������� */
	private MainFrame mainFrame = MainFrame.getMainFrame();
	/** ���������� ������� ������� */
	private ServerCommandsListener serverCommandsListener;

	/**
	 * setter ��� {@link ServerInputTextRunnable#serverCommandsListener}
	 * @param serverCommandsListener
	 */
	public ServerInputTextRunnable(ServerCommandsListener serverCommandsListener) {
		this.serverCommandsListener = serverCommandsListener;
	}

	/**
	 * setter ��� {@link ServerInputTextRunnable#serverInputReader}
	 * @param serverInputReader
	 */
	public void setServerInputReader(BufferedReader serverInputReader) {
		this.serverInputReader = serverInputReader;
	}

	/**
	 * ������������� ������������� �������
	 */
	public void stop() {
		state = false;
	}

	/**
	 * �������� ������������� �������
	 */
	public void run() {
		try {
			state = true;
			while (state) {
				tempStr = serverInputReader.readLine();
				if (tempStr != null) {
					mainFrame.getTextViewPanel().getOutputTextArea()
							.append(tempStr + "\n");
					serverCommandsListener.checkCommand(tempStr);
				} else
					state = false;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
}
