package com.band.ftpclient.connection;

import java.io.BufferedReader;
import java.io.PrintWriter;

/**
 * ������������ ���������� ��������
 * @version 0.1
 */
public class StreamsThreadsAdaptor {
	
	/** ����� ����������� ������ */
	private Thread serverInputTextThread;
	/** ���������� ������� ������ �� ������� */
	private ServerInputTextRunnable serverInputTextRunnable;
	/** ���������� ��������� ��������� */
	private ServerCommandsListener serverCommandsListener;

	/**
	 * �������������� ���������� ��������� ��������� � ����� ������ �� �������
	 */
	public StreamsThreadsAdaptor() {
		createServerCommandsListener();
		createServerInputTextRunnable();
	}

	/**
	 * ��������� ������
	 * @param serverInputReader ����� ������ ������ �� �������
	 * @param outputWriter ����� ����������� ������ �������
	 */
	public void startStreams(BufferedReader serverInputReader,
			PrintWriter outputWriter) {
		serverInputTextRunnable.setServerInputReader(serverInputReader);
		createServerInputTextThread();
		serverInputTextThread.start();
	}

	/**
	 * ������� ���������� ��������� ���������
	 */
	private void createServerCommandsListener() {
		serverCommandsListener = new ServerCommandsListener();
	}

	/**
	 * ������� ���������� ������ ������ �� �������
	 */
	private void createServerInputTextRunnable() {
		serverInputTextRunnable = new ServerInputTextRunnable(
				serverCommandsListener);
	}

	/**
	 * ������� ����� ������ ������ �� �������
	 */
	private void createServerInputTextThread() {
		if (serverInputTextThread != null)
			serverInputTextThread = null;
		serverInputTextThread = new Thread(serverInputTextRunnable);
	}

	/**
	 * getter ��� {@link StreamsThreadsAdaptor#serverInputTextRunnable}
	 * @return {@link StreamsThreadsAdaptor#serverInputTextRunnable}
	 */
	public ServerInputTextRunnable getServerInputTextRunnable() {
		return serverInputTextRunnable;
	}

	/**
	 * setter ��� {@link StreamsThreadsAdaptor#serverInputTextRunnable}
	 * @param serverInputTextRunnable
	 */
	public void setServerInputTextRunnable(
			ServerInputTextRunnable serverInputTextRunnable) {
		this.serverInputTextRunnable = serverInputTextRunnable;
	}
}
